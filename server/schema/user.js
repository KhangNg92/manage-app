import mongoose from "mongoose";
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  codesecurity: { type: Number, required: true },
  role: { type: String, required: true },
  disable: { type: Number },
});

module.exports = mongoose.model("User", userSchema);
