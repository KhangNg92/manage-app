if (process.env.NODE_ENV === "production") require("newrelic");
import express from "express";
import { connectDB } from "./config/db";
import config from "config";
import cors from "cors";
import glob from "glob";
import path from "path";
import route from "./routes/user/index";

const app = express();

// Connect Database
connectDB();

// Init Middleware
app.use(express.json());
// app.use(cors());

// Define Routes
app.use("", route);

// glob(`${__dirname}/routes/*`, {}, (err, files) => {
//   if (err) {
//     throw err;
//   }
//   if (files) {
//     files.map((data) => {
//       let apis = data.split(path.sep).pop();
//       app.use(`/api/${apis}`, data);
//     });
//   }
// });

const PORT = config.get("site.port");

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
