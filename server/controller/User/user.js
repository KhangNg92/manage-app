import User from "../../schema/user";
import { validationResult } from "express-validator";
import _ from "lodash";
import md5 from "md5";
import jwt from "jsonwebtoken";
import config from "config";

const expiresIn = 2592000; // 30 * 24 * 60 (30 day in month)

export const register = async (req, res) => {
  try {
    validationResult(req).throw();
    const { email, password, cornfirmPassword } = req.body;
    let Users = await User.findOne({ email });

    if (Users) {
      return res.status(401).json({
        status: 401,
        message: "Email is exist, please use email another",
        success: false
      });
    }

    if (password != cornfirmPassword) {
      return res.status(401).json({
        status: 401,
        message: "Password doesn't match. Please check password!",
        success: false
      });
    }

    const codesecurity = Math.floor(Math.random() * 10000);
    let passwordHash = md5(md5(password) + codesecurity); //hash password: md5( md5(password) + coderandom )

    let CreateUser = await User.create({
      email,
      password: passwordHash,
      codesecurity,
      role: "User",
      disable: 0
    });

    if (CreateUser)
      return res.status(200).json({
        status: 200,
        message: "Register success",
        success: true
      });
  } catch (err) {
    res.status(400).json(err.errors?.[0] || err.errors);
  }
};
export const login = async (req, res) => {
  try {
    validationResult(req).throw();

    const { email, password } = req.body;

    let Users = await User.findOne({ email });
    if (!Users) {
      return res.status(404).json({
        status: 404,
        message: "Email not found.",
        success: false
      });
    }

    const passwordHash = md5(md5(password) + Users.codesecurity); //hash password: md5( md5(password) + coderandom )

    if (Users.password === passwordHash) {
      let jsons = {
        status: 1,
        message: "Login success",
        success: true,
        isAdmin: false
      };

      let token = jwt.sign(
        {
          _id: Users._id,
          email: Users.email,
          role: Users.role,
          disable: Users.disable
        },
        config.get("jwt.secret"),
        { expiresIn }
      );
      if (Users.role === "admin") {
        return res
          .status(200)
          .json(Object.assign(jsons, { isAdmin: true, token }));
      } else {
        return res
          .status(200)
          .json(Object.assign(jsons, { isAdmin: false, token }));
      }
    }

    if (Users.password !== passwordHash) {
      return res.status(404).json({
        status: 404,
        message: "Password wrong!",
        success: false
      });
    }

    return res
      .status(402)
      .json({ status: 402, message: "Wrong!!!! Please report to BE" });
  } catch (err) {
    console.log(err);
    res.status(400).json(err.errors?.[0] || err.errors);
  }
};

export const loginSocial = async (req, res) => {
  const { email } = req.body;
  try {
    let json = {
      status: 200,
      success: false
    };

    const emailFound = await User.findOne({ email });

    let token = jwt.sign(
      {
        _id: emailFound._id,
        email: emailFound.email,
        role: emailFound.role,
        disable: emailFound.disable
      },
      config.get("jwt.secret"),
      { expiresIn }
    );

    if (emailFound)
      return res.json(
        Object.assign(json, { message: "Login success!", success: true, token })
      );

    const codesecurity = Math.floor(Math.random() * 10000);
    let passwordHash = md5(md5("fromGoogle") + codesecurity);
    await User.create({
      email,
      password: passwordHash,
      codesecurity,
      role: "User",
      disable: 0
    });

    return res.json(
      Object.assign(json, {
        message: "Create account success!",
        success: true,
        isAdmin: false,
        token
      })
    );
  } catch (err) {
    res.status(400).json(err.errors?.[0] || err.errors);
  }
};
