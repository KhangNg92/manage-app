import { body, validationResult } from "express-validator";

export const loginValidation = [
  body("email").isEmail().notEmpty(),
  body("password").notEmpty(),
];

export const registerValidation = [
  body("email").isEmail().notEmpty(),
  body("password").notEmpty(),
  body("cornfirmPassword")
    .notEmpty()
    .withMessage("Cornfirm password is required"),
];
