import jwt from 'jsonwebtoken'
import config from 'config'


const auth = (req) => {
  return new Promise((resolve, reject) => {
    try {
      let token =
      req.headers["x-access-token"] ||
      req.headers["authorization"] ||
      req.cookies.token ||
      null;
      // Express headers are auto converted to lowercase
      if (token) {
        if (token.startsWith("Bearer ")) {
          token = token.slice(7, token.length);
          jwt.verify(token, config.get("jwt.secret"), (error, data) => {
            if (error) {
              reject({
                msg: "Authentication failed! Please check the request",
                success: false,
              });
            }
            resolve(data);
          });
        }
      } else {
        reject({
          msg: "Authentication Emtry! Please check the request",
          success: false,
        });
      }
    } catch (err) {
      reject({
        msg: err.message,
        success: false,
      });
      console.log(err.message);
    }
  });
};

export default auth