import express from "express";

// import { register } from "../../controller/user";
import { loginValidation, login } from "../../controller/User";
import * as User from "../../controller/User";
const route = express.Router();

route.post(
  "/register",
  User.Validation.registerValidation,
  User.UserController.register
);

route.post(
  "/login",
  User.Validation.loginValidation,
  User.UserController.login
);

route.post("/loginWithSocial", User.UserController.loginSocial);

export default route;
