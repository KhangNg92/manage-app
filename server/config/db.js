import mongoose from "mongoose";
import config from 'config';
export const connectDB = async () => {
  try {
    console.log(process.env.NODE_ENV);
    await mongoose.connect(config.get('DB_URL'), {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    });

    console.log("MongoDB Connected...");
  } catch (err) {
    console.error(err.message);
    // Exit process with failure
    process.exit(1);
  }
};
