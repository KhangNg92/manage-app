import React, { useState, useLayoutEffect } from "react";
import { Box, Typography, Avatar } from "@material-ui/core";
import { ArrowForwardIos } from "@material-ui/icons";
import { useStyles } from "./style";
import { ServicesChildProps } from "./services";

const ServiceItem = React.memo((props: any) => {
  const classes = useStyles();
  const { className, item } = props;
  const [width, setWidth] = useState(window.innerWidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", (_e) => {
      setWidth(window.innerWidth);
    });
  });

  return (
    <React.Fragment>
      <Box
        style={{
          width: width - 64,
        }}
        className={classes.boxSliderServicesItem + " " + className}
      >
        <Box display={"flex"} justifyContent={"center"} style={{ flex: 2 }}>
          <Avatar
            style={{
              width: 300,
              height: "auto",
            }}
            variant={"rounded"}
            src={
              item.srcImage ||
              "https://previews.123rf.com/images/dolgachov/dolgachov1412/dolgachov141201828/34425885-beauty-health-people-and-spa-concept-beautiful-young-woman-in-spa-salon-lying-on-the-massage-desk.jpg"
            }
          />
        </Box>

        <Box ml={"8%"} style={{ flex: 4 }}>
          <Box
            border={"1px solid rgba(0,0,0,0.1)"}
            display={"flex"}
            bgcolor={"#fbfbfb"}
            alignItems={"center"}
          >
            <Box padding={1} flex={1} display={"flex"}>
              <Typography
                style={{ marginLeft: 8, color: "#ff67ab" }}
                variant={"h5"}
              >
                {item.title}
              </Typography>
            </Box>
            <Box
              display={"flex"}
              padding={2}
              color={"#fff"}
              bgcolor={"rgba(255,103,171,0.8)"}
            >
              <Typography variant={"caption"}>${item.price || 0}</Typography>
            </Box>
          </Box>

          <Box my={2} color={"#555"}>
            <Typography variant={"subtitle2"}>{item.content}</Typography>
          </Box>

          {(item.listChildServices || []).map(
            (itemChildServices: ServicesChildProps) => {
              return (
                <React.Fragment>
                  <Box className={classes.boxListChildServices}>
                    <Box
                      style={{
                        width: 20,
                        height: 20,
                        color: "rgba(255,103,171,1)",
                      }}
                    >
                      <ArrowForwardIos fontSize={"inherit"} />
                    </Box>
                    <Typography variant={"caption"}>
                      {itemChildServices.title || ""}
                    </Typography>
                  </Box>
                </React.Fragment>
              );
            }
          )}
        </Box>
      </Box>
    </React.Fragment>
  );
});

export default ServiceItem;
