import React, { useState, useLayoutEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import {} from "@material-ui/icons";
import { useStyles } from "./style";
import DataService from "./ServicesData.json";
import { ServicesItem } from "./services";
import ServiceItem from "./ServiceItem";

const ServiceScreen = React.memo((props: any) => {
  const classes = useStyles();
  const [services] = useState<ServicesItem[]>(DataService);
  const [sliderIndex, setSliderIndex] = useState(0);
  const [width, setWidth] = useState(window.innerWidth);

  useLayoutEffect(() => {
    window.addEventListener("resize", (e) => {
      setWidth(window.innerWidth);
    });
  });
  return (
    <React.Fragment>
      <Box className={classes.root}>
        <Box
          className={classes.boxAboutImg}
          display={"flex"}
          justifyContent={"center"}
        >
          <Typography variant={"h5"}>OUR</Typography>
          <Typography
            style={{ marginLeft: 8, color: "#ff67ab" }}
            variant={"h5"}
          >
            SERVICES
          </Typography>
        </Box>

        <Box className={classes.boxServicesList}>
          {(services || []).map((items: ServicesItem, index: number) => {
            return (
              <Box
                onClick={() => setSliderIndex(index)}
                style={{
                  background: items.color || (index % 2 === 0 ? "blue" : "red"),
                }}
                className={
                  classes.boxServiceItems +
                  ` ${
                    sliderIndex === index ? classes.boxServiceItemActive : ""
                  }`
                }
              >
                <Typography align={"center"} variant={"caption"}>
                  {items.title}
                </Typography>
              </Box>
            );
          })}
        </Box>

        <Box
          style={{
            width: DataService.length * width,
            transform: `translate3d(${sliderIndex * -(width - 64)}px, 0, 0)`,
          }}
          className={classes.boxSliderServices}
        >
          {(DataService || []).map((item, index) => {
            return (
              <ServiceItem
                item={item}
                className={
                  index === sliderIndex
                    ? classes.boxSliderServicesItemActive
                    : ""
                }
                index={index}
              />
            );
          })}
        </Box>
      </Box>
    </React.Fragment>
  );
});

export default ServiceScreen;
