import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 4),
    overflow: "hidden",
  },
  boxAboutImg: {
    margin: theme.spacing(4, 0),
  },
  boxServicesList: {
    display: "flex",
    justifyContent: "space-between",
  },
  boxServiceItems: {
    margin: 1,
    padding: theme.spacing(2, 3),
    flex: 1,
    color: "rgba(255,255,255,1)",
    textAlign: "center",
    fontWeight: 600,
    "&:hover": {
      background: "#493c41 !important",
      cursor: "pointer",
    },
  },
  boxServiceItemActive: {
    background: "#493c41 !important",
  },
  boxSliderServices: {
    display: "flex",
    transition: "all 300ms ease-out",
  },
  boxSliderServicesItem: {
    height: 350,
    transition: "all 300ms ease-out",
    opacity: 0,
    display: "flex",
    padding: theme.spacing(2, 0),
  },
  boxSliderServicesItemActive: {
    width: 1024 - 64,
    opacity: 1,
  },
  boxListChildServices: {
    display: "flex",
    borderBottom: "1px solid #ccc",
    alignItems: "center",
    transition: "all 0.3s ease-in",
    marginTop: theme.spacing(0.5),
    color: "#555",
    "&:hover": {
      background: "#fbfbfb",
      transform: "translateX(10px)",
    },
  },
}));
