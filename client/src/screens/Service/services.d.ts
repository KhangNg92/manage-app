export interface ServicesItem {
  title?: string;
  price?: number;
  color?: string;
  content?: string;
  srcImage?: string;
  listChildServices?: Array<ServicesChildProps>[];
}

export interface ServicesChildProps {
  title?: string;
}
