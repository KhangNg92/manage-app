import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(theme => ({
  root: {
    background: "#fbfbfb",
    border: "1px solid rgba(0,0,0,0.1)",
    padding: theme.spacing(2, 4)
  },
  boxAboutImg: {
    margin: theme.spacing(4, 0)
  },
  aboutImg: {
    width: 450,
    height: "auto",
    margin: "0 auto",
    position: "relative"
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    "&:focus": {
      border: "none"
    }
  }
}));
