import React from "react";
import { Box, Typography, Avatar } from "@material-ui/core";
import {} from "@material-ui/icons";
import { useStyles } from "./style";

const AboutScreen = React.memo((props: any) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Box className={classes.root}>
        <Box className={classes.boxAboutImg}>
          <Avatar
            className={classes.aboutImg}
            variant={"square"}
            src={
              "https://previews.123rf.com/images/dolgachov/dolgachov1412/dolgachov141201828/34425885-beauty-health-people-and-spa-concept-beautiful-young-woman-in-spa-salon-lying-on-the-massage-desk.jpg"
            }
          />
        </Box>

        <Box className={classes.boxAboutImg} display={'flex'} justifyContent={'center'}>
          <Typography variant={'h5'}>
            ABOUT
          </Typography>
          <Typography style={{marginLeft: 8, color:'#ff67ab'}} variant={'h5'}>
              BEAUTYSPA
            </Typography>
        </Box>

        <Box className={classes.boxAboutImg}>
          <Typography style={{fontWeight: 200, color: '#555'}}  align={"center"} variant={"subtitle2"}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
            gravida velit quis dolor tristique accumsan. Pellentesque elit
            tortor, adipiscing vel velit in, ultriciesnulla. Donec in urna sem.
            Nulla facilisiestibulum ut aliquet agna. Nulla nec faucibus est. In
            in augue placerat, ligula quis, elementum augue. Lorem ipsum dolor
            sit amet, consectetur adipiscing elit. Integer gravida velit quis
            dolor tristique accumsan. Pellentesque lla nec faucibus est. In in
            augue placerat, ligula quis, elementum augue.
          </Typography>
        </Box>
      </Box>
    </React.Fragment>
  );
});

export default AboutScreen;
