import React, { FC, useState } from "react";
import FacebookLogin from "react-facebook-login";

import { GoogleLogin } from "react-google-login";
import {
  Modal,
  Typography,
  Input,
  InputAdornment,
  IconButton
} from "@material-ui/core";
import { useStyles } from "../About/style";
import GoogleButton from "react-google-button";
import ButtonIcon from "../../Components/Button/ButtonIcon";
import { Visibility, VisibilityOff } from "@material-ui/icons";

import axios from "axios";
import { connect, useDispatch, useSelector } from "react-redux";
import { loginWithSocial } from "../../redux/slices/userSlice";
import { RootState } from "../../redux/rootReducer";

type RegisterProps = {
  open: boolean;
  handleClose: Function;
};

const Register: FC<RegisterProps> = ({ open, handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userInfo = useSelector((state: RootState) => state.user);

  const [showPassword, setShowPassword] = useState(false);

  const handleLoginCall = async (dataToSend: any) => {
    await dispatch(loginWithSocial(dataToSend));
    return !userInfo.error.length && handleClose(false);
  };
  return (
    <Modal
      open={open}
      onClose={() => handleClose()}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div
        className={classes.paper}
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%,-50%)",
          minHeight: 400,
          border: "none",
          display: "flex",
          flexDirection: "column"
        }}
      >
        <form action="" style={{ display: "flex", flexDirection: "column" }}>
          <input type="email" style={{ margin: "10px 0px", padding: 20 }} />
          <Input
            id="standard-adornment-password"
            type={showPassword ? "text" : "password"}
            value={""}
            onChange={() => {}}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowPassword(!showPassword)}
                  // onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
        </form>

        <div>
          <ButtonIcon
            title="Login"
            variant="contained"
            style={{ width: "100%", margin: "10px 0px" }}
          />

          <Typography align="center" style={{ margin: "10px 0px" }}>
            ---------- OR ---------
          </Typography>
          <FacebookLogin
            appId="313559466674946"
            // autoLoad={true}
            fields="name,email,picture"
            onClick={console.log}
            callback={console.log}
            buttonStyle={{ marginBottom: 10, fontSize: 12, width: "100%" }}
            icon="fa-facebook"
            // isDisabled={false}
          />
        </div>

        <GoogleLogin
          clientId="453414998190-mrjhem4t1j4444jua6brd0uohpidgrt4.apps.googleusercontent.com"
          buttonText="Login With Google"
          onSuccess={val => handleLoginCall(val)}
          onFailure={console.log}
          cookiePolicy={"single_host_origin"}
          render={({ onClick }) => (
            <GoogleButton
              style={{ width: "100%", background: "#ff0000" }}
              onClick={() => onClick()}
            />
          )}
        />
      </div>
    </Modal>
  );
};

export default connect()(Register);
