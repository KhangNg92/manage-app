import React from 'react'

export interface RouteConfigs{
    path?: string
    component?: React.FunctionComponent<any>| undefined
    exact?: boolean
    private?: boolean
}
