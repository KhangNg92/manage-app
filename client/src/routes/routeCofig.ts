import { RouteConfigs } from "./RoutesConfig.d";
import * as Screens from "../screens";

const PublicRoutes: RouteConfigs[] = [
  {
    component: Screens.About,
    exact: true,
    path: "/"
  },
  {
    component: Screens.Register,
    exact: true,
    path: "/register"
  }
];

const PrivateRoutes: RouteConfigs[] = [
  {
    component: Screens.About,
    exact: true,
    path: "/admin",
    private: true
  }
];

export default [...PublicRoutes, ...PrivateRoutes];
