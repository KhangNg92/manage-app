import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import { createBrowserHistory } from "history";
import RouteConfig from "./routeCofig";
import Layout from "../layout";
import { Provider } from "react-redux";
import store, { persistor } from "../redux";
import { PersistGate } from "redux-persist/integration/react";

const history = createBrowserHistory();

const Routes = () => {
  const redirectView = () => {
    const route =
      RouteConfig.find(f => f.path === history.location.pathname) || {};

    const token = null;
    if (route.private && !token) {
      return <Redirect exact to={"/"} />;
    }
  };

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <React.Fragment>
          <Router>
            <Layout>
              <Switch>
                <React.Fragment>
                  {RouteConfig.map(r => {
                    return (
                      <Route
                        key={r.path}
                        exact={r.exact}
                        path={r.path}
                        component={r.component}
                      />
                    );
                  })}

                  {redirectView()}
                </React.Fragment>
              </Switch>
            </Layout>
          </Router>
        </React.Fragment>
      </PersistGate>
    </Provider>
  );
};

export default Routes;
