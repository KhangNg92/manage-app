import { combineReducers } from "redux";

import userSliceReducer from "./slices/userSlice";

const rootReducer = combineReducers({
  user: userSliceReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
