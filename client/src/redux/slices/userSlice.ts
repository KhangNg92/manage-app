import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "..";
import jwt from "jsonwebtoken";
import axios from "axios";

interface User {
  email: string;
  password: string;
  role: string;
  disable: number;
  codesecurity: number;
}

interface UserState {
  user: User;
  isLoading: boolean;
  error: string;
}

const initialState: UserState = {
  user: {
    email: "",
    password: "",
    role: "",
    disable: 0,
    codesecurity: 0
  },
  isLoading: false,
  error: ""
};

function startLoading(state: UserState) {
  state.isLoading = true;
}

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    getUserStart: startLoading,
    getUserSuccess: (state, action: PayloadAction<any>) => {
      state.isLoading = false;
      state.user = action.payload;
    },
    getUserFailure: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    logOutUser: (state: UserState) => {
      state.user = initialState.user;
    }
  }
});

export const {
  getUserStart,
  getUserSuccess,
  getUserFailure,
  logOutUser
} = userSlice.actions;

export default userSlice.reducer;

// ACTIONS
export const loginWithSocial = (user: any): AppThunk => async dispatch => {
  try {
    dispatch(getUserStart());
    const {
      data: { token }
    } = await axios.post("/loginWithSocial", {
      email: user.profileObj.email
    });

    console.log(token);

    const decodedData = jwt.decode(token);
    // console.log(decodedData);
    dispatch(getUserSuccess(decodedData));
  } catch (err) {
    dispatch(getUserFailure(err));
  }
};
