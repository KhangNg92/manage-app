import React, { useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import { ArrowUpward as ArrowUpIcons } from "@material-ui/icons";
import { useStyles } from "./style";
import SliderComponent from "../Components/Slider";
import Header from "../Components/Header";
import Data from "../fakeData.json";
import ServiceScreen from "../screens/Service";

const Layout = ({ children }: any) => {
  const classes = useStyles();
  const [isShow, setIsShow] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", handleScrollShowIcon);
  }, []);

  const handleScrollShowIcon = (): void => {
    if (window.scrollY > window.innerHeight + (window.innerHeight * 10) / 100) {
      setIsShow(true);
    } else {
      setIsShow(false);
    }
  };

  const handleScrollTop = (): void => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
  };
  return (
    <Box className={classes.root}>
      <SliderComponent
        logo={
          "https://an-spa.vn/wp-content/uploads/2018/12/48378089_378511189364911_9044264199735214080_n.png"
        }
        data={Data}
        duration={5000}
      />
      <Header />
      <main>{children}</main>
      <ServiceScreen />

      <Box
        onClick={handleScrollTop}
        className={classes.iconsUp + ` ${isShow ? classes.showIcons : ""}`}
      >
        <ArrowUpIcons />
      </Box>
    </Box>
  );
};

export default Layout;
