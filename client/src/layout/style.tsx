import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  root: {},
  iconsUp: {
    position: "fixed",
    bottom: 10,
    right: 30,
    background: "rgba(255,103,171,0.8)",
    padding: theme.spacing(1.5, 2),
    visibility: "hidden",
    opacity: 0.8,
    color: "rgba(255,255,255)",
    transition: "all 0.4s ease-in",
    animation: "$hide-animation 0.4s ease-in",
    "&:hover": {
      background: "rgba(255,103,171,1)",
      cursor: "pointer",
      opacity: 1,
    },
  },
  showIcons: {
    visibility: "visible",
    animation: "$show-animation 0.4s ease-in",
  },

  "@keyframes show-animation": {
    "0%": {
      opacity: 0,
      transform: "translateY(20px)",
    },
    "100%": {
      opacity: 1,
      transform: "translateY(0)",
    },
  },

  "@keyframes hide-animation": {
    "0%": {
      opacity: 1,
      transform: "translateY(0)",
    },
    "100%": {
      opacity: 0,
      transform: "translateY(20px)",
    },
  },
}));
