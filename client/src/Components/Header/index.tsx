import React, { useState, useEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import {
  Facebook as FacebookIcon,
  YouTube as YouTubeIcon,
  Twitter as TwitterIcon,
  Instagram as InstagramIcon
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useStyles } from "./style";
import { Register } from "../../screens";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";

import { logOutUser } from "../../redux/slices/userSlice";

interface narbarProps {
  name: string;
  to: string;
  icon?: string;
  onClick?: Function;
}

interface navbarIconsProps {
  Component: any;
  to?: string;
  name?: string;
}

const Header = React.memo((props: any) => {
  const [isFixed, setFixed] = useState<boolean>(false);
  const [navbar, setNavBar] = useState<narbarProps[]>([
    { name: "Home", to: "/" },
    { name: "About", to: "/link" },
    { name: "Service", to: "/services" },
    { name: "Offer", to: "/offer" },
    { name: "Team", to: "/team" }
  ]);

  const [showmodal, setShowmodal] = useState(false);
  const [navbarIcons] = useState<navbarIconsProps[]>([
    { Component: <FacebookIcon />, name: "Facebook" },
    { Component: <YouTubeIcon />, name: "Youtube" },
    { Component: <TwitterIcon />, name: "Twitter" },
    { Component: <InstagramIcon />, name: "Instagram" }
  ]);

  // Redux
  const userInfo = useSelector((state: RootState) => state.user.user);
  const dispatch = useDispatch();

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    const isLoggedIn = userInfo.email.length || userInfo === null;
    const navBarNew = [...navbar];

    if (!isLoggedIn) {
      const navbarLogout = navBarNew.findIndex(({ name }) => name === "Logout");
      const navbarFilter = navBarNew.filter(({ name }) => name !== "Admin");
      navbarFilter.splice(navbarLogout, 1, {
        name: "Register/Login",
        to: "",
        onClick: () => setShowmodal(true)
      });
      return setNavBar(navbarFilter);
    }

    if (isLoggedIn) {
      const registerMenu = navBarNew.findIndex(
        ({ name }) => name === "Register"
      );

      navbar.splice(registerMenu, 1, {
        name: "Logout",
        to: "",
        onClick: () => dispatch(logOutUser())
      });

      if (userInfo.role === "Admin")
        return setNavBar([...navbar, { name: "Admin", to: "/admin" }]);
    }
  }, [userInfo]);

  const handleScroll = (): void => {
    if (window.scrollY > window.innerHeight) {
      setFixed(true);
    } else {
      setFixed(false);
    }
  };
  const classes = useStyles();

  const displayMenuItem = (
    name: string,
    to: string,
    index: number,
    onClick: Function
  ) => {
    return !to.length ? (
      <div
        key={index.toString()}
        className={classes.navBarItem}
        onClick={() => onClick()}
      >
        <Typography>{name}</Typography>
      </div>
    ) : (
      <Link
        key={index.toString()}
        to={to}
        style={{ textDecoration: "none" }}
        className={classes.navBarItem}
      >
        <Typography>{name}</Typography>
      </Link>
    );
  };

  return (
    <React.Fragment>
      <Box
        className={classes.root}
        style={{ position: isFixed ? "sticky" : "relative" }}
      >
        <Box className={classes.navbar}>
          {(navbar || []).map(({ name, to, onClick }, index) =>
            displayMenuItem(name, to, index, onClick as Function)
          )}
        </Box>

        <Box className={classes.navbarIcons}>
          {(navbarIcons || []).map((item, index) => (
            <Box key={index.toString()} className={classes.navBarIconItem}>
              {item.Component}
            </Box>
          ))}
        </Box>
      </Box>

      <Register open={showmodal} handleClose={setShowmodal} />
    </React.Fragment>
  );
});

export default Header;
