import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  root: {
    height: theme.spacing(8),
    width: '100%',
    background: "#3b3035",
    display: "flex",
    justifyContent: "space-between",
    top: 0,
    zIndex: 3,
    transition: 'all 0.7s ease-in',
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      height: theme.spacing(16),
      alignItems: 'center',
    },
  },
  navbar: {
    display: "flex",
    height: "100%",
  },
  navbarIcons: {
    display: "flex",
  },
  navBarItem: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(1.5, 2),
    color: "rgba(255,255,255)",
    transition: "all 0.4s linear",
    "&:hover": {
      cursor: "pointer",
      background: "#ff67ab",
    },
  },
  navBarIconItem: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(1.5, 2),
    color: "rgba(255,255,255)",
    transition: "all 0.4s linear",
    "&:hover": {
      cursor: "pointer",
      color: "#ff67ab",
    },
  }
}));
