import React, { useEffect, useState } from "react";
import { Box, Avatar, Typography } from "@material-ui/core";
import {
  NavigateNext as NextIcons,
  NavigateBefore as PreIcons,
} from "@material-ui/icons";
import ButtonRound from "../Button/ButtonRound";
import { useStyles } from "./style";
import { BgSlider } from "../../assets/images";

interface DataSlider {
  content?: string;
  title?: string;
  bgUrl?: string;
}

interface SliderProps {
  data: Array<DataSlider>;
  logo?: string;
  duration?: number;
}

const SliderComponent = (props: SliderProps) => {
  const { data, logo, duration } = props;

  const classes = useStyles();

  const [sliderIndex, setSliderIndex] = useState<number>(0);
  var timer: any = null;

  const sliders = (): void => {
    timer = setTimeout(() => {
      if (sliderIndex === data.length - 1) {
        setSliderIndex(0);
      } else {
        setSliderIndex(sliderIndex + 1);
      }
    }, duration);
  };
  useEffect(() => {
    sliders();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [duration, sliderIndex]);

  const btnIcons = (type: string): void => {
    clearTimeout(timer);
    switch (type) {
      case "next":
        if (sliderIndex === data.length - 1) {
          setSliderIndex(0);
        } else {
          setSliderIndex(sliderIndex + 1);
        }
        break;
      case "pre":
        if (sliderIndex === 0) {
          setSliderIndex(data.length - 1);
        } else {
          setSliderIndex(sliderIndex - 1);
        }
        break;
      default:
        return;
    }
  };

  return (
    <Box className={classes.container}>
      <Box
        onClick={() => btnIcons("pre")}
        className={classes.boxIcons}
        style={{ left: 0 }}
      >
        <PreIcons className={classes.icons} />
      </Box>

      <Box
        onClick={() => btnIcons("next")}
        className={classes.boxIcons}
        style={{ right: 0 }}
      >
        <NextIcons className={classes.icons} />
      </Box>
      {logo && (
        <Box className={classes.boxLogo}>
          <Avatar
            className={classes.imgLogo}
            variant={"rounded"}
            src={
              logo ||
              "https://i.pinimg.com/originals/12/b9/f6/12b9f6c7963992316126a1df5319f7fd.jpg"
            }
          />
        </Box>
      )}
      {(data || []).map((item, index) => {
        return (
          <Box
            key={index.toString()}
            className={
              classes.wrapSlider +
              ` ${index === sliderIndex ? classes.activeSlider : ""}`
            }
            style={{ backgroundImage: `url(${item.bgUrl || ""})` }}
          >
            <Box
              style={{
                background: `url(${BgSlider}) repeat 0 0`,
                position: "absolute",
                width: "100%",
                height: "100%",
                zIndex: 0,
              }}
            />
            <Box className={classes.slideCaption}>
              <Box zIndex={1} className={classes.boxTitle}>
                <Typography style={{ fontWeight: "bold" }} variant={"h4"}>
                  {item.title}
                </Typography>
              </Box>

              <Box zIndex={1} mb={1}>
                <Typography
                  style={{ fontWeight: "bold", textAlign: "center" }}
                  variant={"body2"}
                >
                  {item.content || ""}
                </Typography>
              </Box>

              <Box>
                <ButtonRound
                  style={{ background: "#ff67ab", color: "#fff" }}
                  title={"Read More"}
                />
              </Box>
            </Box>
          </Box>
        );
      })}
    </Box>
  );
};

SliderComponent.defaultProps = {
  duration: 3000,
};
export default SliderComponent;
