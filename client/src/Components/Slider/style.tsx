import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  container: {
    width: "100%",
    height: "100vh", //get full height screen
    display: "flex",
    position: "relative",
    background: "rgba(0,0,0,0.3)",
    overflow: 'hidden'
  },
  boxIcons: {
    background: "rgba(0, 0, 0, 0.3)",
    width: theme.spacing(8),
    height: theme.spacing(8),
    position: "absolute",
    top: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "rgba(255, 255, 255, 0.5)",
    zIndex: 1,
    "&:hover": {
      background: "rgba(0, 0, 0, 0.8)",
      cursor: "pointer",
    },
  },
  boxLogo: {
    width: "100vw",
    position: "absolute",
    justifyContent: "center",
    display: "flex",
    top: 0,
    zIndex: 1,
  },
  wrapSlider: {
    width: "100vw",
    color: "rgba(255,255,255)",
    alignItems: "center",
    justifyContent: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    display: "none",
    transition: "all 1s ease-in",
    animation: "$intro-animation 1s ease-in",
  },
  activeSlider: {
    display: "block",
  },
  slideCaption: {
    margin: "0 auto",
    maxWidth: 960,
    height: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    zIndex: 2,
    justifyContent: "center",
  },
  boxTitle: {
    borderTop: "1px solid #fff",
    borderBottom: "1px solid #fff",
    borderWidth: 1,
    padding: theme.spacing(2, 0),
    marginBottom: theme.spacing(1),
  },
  imgLogo: {
    width: theme.spacing(30),
    height: "auto",
    padding: theme.spacing(1, 2),
    maxHeight: theme.spacing(25),
  },
  icons: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },

  "@keyframes intro-animation": {
    "0%": {
      opacity: 0,
    },
    "100%": {
      opacity: 1,
    },
  },
}));
