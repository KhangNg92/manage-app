import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles((theme) => ({
  btnActions: {
    minWidth: 0,
    margin: theme.spacing(1),
    padding: theme.spacing(1, 1.5),
  },
  btnRound: {
    borderRadius: theme.spacing(12.5),
  },
}));
export default useStyle;
