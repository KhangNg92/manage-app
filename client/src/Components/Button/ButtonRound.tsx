import React from "react";
import { Button } from "@material-ui/core";
import {} from "@material-ui/icons";
import useStyle from "./style";

const ButtonRound = (props: any) => {
  const classes = useStyle();
  const {
    style,
    className,
    onClick,
    title,
    leftIcon,
    rightIcon,
    color,
    variant,
  } = props;
  return (
    <React.Fragment>
      <Button
        style={style}
        variant={variant}
        className={
          classes.btnActions + " " + classes.btnRound + " " + className
        }
        color={color}
        onClick={onClick || null}
      >
        {leftIcon}
        {title}
        {rightIcon}
      </Button>
    </React.Fragment>
  );
};

ButtonRound.defaultProps = {
  variant: "text",
};

export default ButtonRound;
