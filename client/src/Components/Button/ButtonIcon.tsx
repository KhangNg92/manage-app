import React from "react";
import { Button } from "@material-ui/core";
import {} from "@material-ui/icons";
import useStyle from "./style";

const ButtonIcon = (props: any) => {
  const {
    className,
    onClick,
    title,
    leftIcon,
    rightIcon,
    color,
    variant,
    style
  } = props;
  const classes = useStyle();

  return (
    <React.Fragment>
      <Button
        variant={variant}
        className={classes.btnActions + " " + className}
        color={color}
        onClick={onClick || null}
        style={style}
      >
        {leftIcon}
        {title}
        {rightIcon}
      </Button>
    </React.Fragment>
  );
};

ButtonIcon.defaultProps = {
  variant: "text"
};

export default ButtonIcon;
